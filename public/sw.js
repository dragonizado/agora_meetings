// This is the "Offline page" service worker
const ST_DOMAIN = "/"
const CACHE = "agoraoffline-page";

const offlineFallbackPage = "offline/offline.html";
const offlineFallbackPage_ext = [
ST_DOMAIN+"offline/images/icons/favicon.ico",
ST_DOMAIN+"offline/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
ST_DOMAIN+"offline/css/util.css",
ST_DOMAIN+"offline/css/main.css",
ST_DOMAIN+"offline/fonts/Poppins/Poppins-Regular.ttf",
ST_DOMAIN+"offline/fonts/Lato/Lato-Bold.ttf",
ST_DOMAIN+"offline/fonts/Lato/Lato-Regular.ttf",
ST_DOMAIN+"offline/fonts/Poppins/Poppins-Bold.ttf"
];

self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

self.addEventListener('install', async (event) => {
  event.waitUntil(
    caches.open(CACHE)
      .then((cache) => {
          cache.add(offlineFallbackPage)
          cache.addAll(offlineFallbackPage_ext)
        })
  );
});

// if (workbox.navigationPreload.isSupported()) {
//   workbox.navigationPreload.enable();
// }

self.addEventListener('fetch', (event) => {

  if (event.request.mode === 'navigate') {
    event.respondWith((async () => {
      try {
        const preloadResp = await event.preloadResponse;

        if (preloadResp) {
          return preloadResp;
        }

        const networkResp = await fetch(event.request);
        return networkResp;
      } catch (error) {

        const cache = await caches.open(CACHE);
        let cachedResp = await cache.match(offlineFallbackPage);

        return cachedResp;
      }
    })());


  }else{
      if (event.request.url.includes('/offline/')){
          event.respondWith(caches.open(CACHE).then((ob)=>{
              return ob.match(event.request.url).then( res=>res )
          }))

      }
  }
});
