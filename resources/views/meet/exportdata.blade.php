<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Export meet') }}
        </h2>
    </x-slot>
    <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="overflow-hidden pb-2">
                    <form action="{{ route("meet.exportexcel") }}" method="post">
                        @csrf
                            <div class="bg-white ">
                                <div class="p-3">
                                    <h2>Usuarios</h2>
                                </div>
                                <hr>
                                <div class="p-3">
                                    @foreach ($users as $user)
                                        @if ($user->id != 1)
                                            <div class="flex items-start">
                                                <div class="flex items-center h-5">
                                                    <input type="checkbox" value="{{$user->id}}" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" name="usersid[]" id="users-{{$user->id}}">
                                                </div>
                                                <div class="ml-3 text-sm">
                                                    <label for="users-{{$user->id}}" class="font-medium text-gray-700">{{ $user->name }}</label>
                                                    <p class="text-gray-500">{{ $user->email }}</p>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                            <div class="bg-white ">
                                <div class="pt-3 pl-3">
                                    <h2>Configuración</h2>
                                </div>
                                <div class="flex w-full">
                                    <div class="col-span-6 w-1/2 mt-3 p-3 sm:col-span-3">
                                        <label for="datein" class="block text-sm font-medium text-gray-700">fecha desde</label>
                                        <input type="date" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="desde" name="datein" id="datein" required>
                                    </div>
                                    <div class="col-span-6 w-1/2 mt-3 p-3 sm:col-span-3">
                                        <label for="dateout" class="block text-sm font-medium text-gray-700">fecha hasta</label>
                                        <input type="date" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="hasta" name="dateout" id="dateout" >
                                    </div>
                                </div>
                                <div class="p-3">
                                    <button class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 w-full mt-3">Exportar</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
</x-app-layout>
