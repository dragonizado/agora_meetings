<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create new meetings') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden pb-2">
                <x-jet-form-section submit="createTeam">
                    <x-slot name="title">
                        {{ __('Meetings Details') }}
                    </x-slot>

                    <x-slot name="description">
                        {{ __('Create a new meetings to register participants') }}
                    </x-slot>

                    <x-slot name="form">

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="name" value="{{ __('Topic') }}" />
                            <x-jet-input id="name" type="text" class="mt-1 block w-full"/>
                            <x-jet-input-error for="name" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="name" value="{{ __('Objective') }}" />
                            <x-jet-input id="name" type="text" class="mt-1 block w-full" />
                            <x-jet-input-error for="name" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="name" value="{{ __('Agora code') }}" />
                            <x-jet-input id="name" type="text" class="mt-1 block w-full" />
                            <x-jet-input-error for="name" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="name" value="{{ __('Observations') }}" />
                            <textarea name="" id="" class="form-input rounded-md shadow-sm mt-1 block w-full" cols="30" rows="10"></textarea>
                            <x-jet-input-error for="name" class="mt-2" />
                        </div>

                         
                    </x-slot>

                    <x-slot name="actions">
                        <x-jet-button>
                            {{ __('Create') }}
                        </x-jet-button>
                    </x-slot>
                </x-jet-form-section>
            </div>
        </div>
    </div>
</x-app-layout>