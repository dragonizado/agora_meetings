<html>
    <head></head>
    <body>
        <table>
            <tr>
                <th>Identificador de reunión</th>
                <th>Nombre Formación</th>
                <th>Objectivo</th>
                <th>Estado</th>
                <th>Observaciones</th>
                <th>Fecha y Hora de inicio</th>
                <th>Fecha y Hora de finalización</th>
                <th>Duración</th>
                <th>Contador participantes</th>
                <th>Nombre participante</th>
                <th>Documento participante</th>
                <th>Area participante</th>
                <th>EMPLEADO ESPUMRED - Nombre</th>
                <th>EMPLEADO ESPUMRED - Documento</th>
                <th>EMPLEADO ESPUMRED - Area</th>
                <th>Participo en la reunión?</th>
            </tr>
            @foreach ($meets as $meet)
            @php
                $participants = $meet->participants()->get();
                $cc = array_column($participants->toArray(),"dni");
                $finish = false;
                $plimit = count($cc);
                $pstep = 0;
            @endphp
                <tr>
                    <td style="background:#BFBFBF;">{{$meet->id}}</td>
                    <td style="background:#BFBFBF;">{{$meet->topic}}</td>
                    <td style="background:#BFBFBF;">{{$meet->objective}}</td>
                    <td style="background:#BFBFBF;">{{$meet->state}}</td>
                    <td style="background:#BFBFBF;">{{$meet->observations}}</td>
                    <td style="background:#BFBFBF;">{{$meet->start_date}}</td>
                    <td style="background:#BFBFBF;">{{$meet->end_date}}</td>
                    <td style="background:#BFBFBF;">{{$meet->num_duration}} {{$meet->type_duration}}</td>
                    <td style="background:#BFBFBF;">{{ count($participants)}}</td>
                    <td style="background:#BFBFBF;"></td>
                    <td style="background:#BFBFBF;"></td>
                    <td style="background:#BFBFBF;"></td>
                    <td style="background:#FCD5B4;"></td>
                    <td style="background:#FCD5B4;"></td>
                    <td style="background:#FCD5B4;"></td>
                    <td style="background:#FCD5B4;"></td>
                </tr>
                   
                @foreach ($apiemployes as $employer)
                    @php
                        $isinaaray = in_array($employer->cc,$cc);

                        //set colors
                        if($isinaaray){
                            $bgcolor = "#C4D79B";
                            $textcolor = "#";
                        }else{
                            $bgcolor = "#FCD5B4";
                            // $textcolor = "#";
                        }
                    @endphp
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    @if (!$finish)
                    <td>{{$participants[$pstep]->full_name  }}</td>
                    @else
                    <td></td>
                    @endif
                    @if (!$finish)
                        <td>{{$participants[$pstep]->dni  }}</td>
                    @else
                        <td></td>
                    @endif
                    @if (!$finish)
                        <td>{{$participants[$pstep]->area  }}</td>
                    @else
                        <td></td>
                    @endif
                    <td style="background:{{$bgcolor}};">{{$employer->nombre_completo}}</td>
                    <td style="background:{{$bgcolor}};">{{$employer->cc}}</td>
                    <td style="background:{{$bgcolor}};">{{ "@".$employer->nombreArea}}</td>
                    <td style="background:{{$bgcolor}};">
                        @if ($isinaaray)
                            SI
                        @else
                            NO
                        @endif
                    </td>
                </tr>
                    @php
                        if($pstep < ($plimit-1)){
                            $pstep++;
                        }else{
                            $finish = true;
                        }
                    @endphp
                @endforeach
                


                 {{-- @foreach ($meet->participants()->get() as $participan)
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $participan->full_name }}</td>
                                <td>{{ $participan->dni }}</td>
                                <td>{{ $participan->area }}</td>
                                <td></td>
                            </tr>
                        @endforeach --}}
                @endforeach

        </table>
    </body>
</html>