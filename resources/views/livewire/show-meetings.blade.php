<x-slot name="header">
    <div class="flex">
        <div class="w-1/2">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ $Meet->topic }} ({{ $Meet->state }})
                </h2>
                <div class="text-sm text-gray-500">{{ $Meet->objective }}  <strong>    Creado:</strong> {{ $Meet->created_at->diffForHumans()}} - {{ $Meet->created_at->format('Y-m-d h:i a')}}</div>
        </div>
        <div class="w-1/2 ">
            <div class="float-right">
                <a href="{{ route('dashboard') }}" class="py-2 px-4 rounded bg-green-500 hover:bg-green-700 text-white" >{{ __('Close') }}</a>
            </div>
        </div>
    </div>
</x-slot>


<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="overflow-hidden pb-2">
            <div class="bg-white px-4 py-3 sm:px-6">

                <input type="text" class="form-input rounded-md shadow-sm mt-1 block w-full" placeholder="{{__('Search By :attribute',['attribute'=>'Nombre'])}}" wire:model="search">
            </div>
            <x-table class="w-full">
                <x-slot name="thead">
                    <tr>
                        <x-th-standar>{{__('SAP_Code')}}</x-th-standar>
                        <x-th-standar>{{__('Full name')}}</x-th-standar>
                        <x-th-standar>{{__('Dni')}}</x-th-standar>

                        <x-th-standar>{{__('Area')}}</x-th-standar>
                        @if ($Meet->request_temp)
                            <x-th-standar>{{__('Temp')}}</x-th-standar>
                        @endif

                        <x-th-standar>{{__('Firm')}}</x-th-standar>
                        <x-th-standar>{{__('In hour')}}</x-th-standar>
                    </tr>
                </x-slot>
                <x-slot name="tbody">
                        @forelse ($participants as $participant)
                            @php
                                    $temp = null;
                                    if($Meet->request_temp){
                                        if(isset($participant->pivot->temp)){
                                            $temp = $participant->pivot->temp;
                                        }

                                    }
                                @endphp
                                <tr>
                                    <x-table-meet-participants
                                    sapcode="{{ $participant->code }}"
                                    fullname="{{ $participant->full_name }}"
                                    dni="{{ $participant->dni }}"
                                    area="{{ $participant->area }}"
                                    firm="{{ $participant->firm }}"
                                    temp="{{ $temp }}"
                                    gettemp="{{$Meet->request_temp}}"
                                    ></x-table-meet-participants>
                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{$participant->pivot->created_at->format('h:i a')}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">
                                        <p class="font-semibold my-4 ">No hay registros </p>
                                    </td>
                                </tr>
                            @endforelse
                </x-slot>
            </x-table>
            {{$participants->links()}}
        </div>
    </div>
</div>
