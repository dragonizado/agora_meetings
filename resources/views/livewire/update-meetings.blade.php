<x-slot name="header">
    <div class="flex">
        <div class="w-1/2">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ $Meet->topic }}
                </h2>
                <div class="text-sm text-gray-500">{{ $Meet->objective }}</div>
        </div>
        <div class="w-1/2 ">
            <div class="float-right">
                <a href="{{ route('dashboard') }}" onclick="return confirm('Deseas salir de la reunión? esta seguira abierta hasta que sea finalizada.')" class="py-2 px-4 rounded bg-red-500 hover:bg-red-700 text-white">{{ __('Cancel') }}</a>
                <button onclick="closeMeet()" class="py-2 px-4 rounded bg-green-500 hover:bg-green-700 text-white " >{{ __('Finish') }}</button>
            </div>
        </div>
    </div>
</x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden pb-2 flex flex-wrap">
                <div class="w-full md:w-1/2 ">

                    <div class="w-full flex justify-center">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_3" x="0px" y="0px" viewBox="0 0 64 64" style="enable-background:new 0 0 64 64;" xml:space="preserve" width="300" height="300"  class="mt-4 bg-green-600 p-8 rounded-lg">
                            <g>
                                <path d="M42,13h-7v-1c0-0.459-0.312-0.859-0.757-0.97l-8-2c-0.317-0.078-0.653,0.001-0.899,0.216   c-0.246,0.215-0.373,0.536-0.337,0.861l3,28C28.06,38.614,28.489,39,29,39h5c0.552,0,1-0.447,1-1v-1h8v5c0,0.553,0.448,1,1,1h1v6   h-1c-0.552,0-1,0.447-1,1v8c0,2.757,2.243,5,5,5h10c0.552,0,1-0.447,1-1V37h1c1.654,0,3-1.346,3-3C63,22.42,53.58,13,42,13z M33,37   h-3.102l-2.751-25.683L33,12.781V14v22V37z M47,43h2v6h-2V43z M57,61h-9c-1.654,0-3-1.346-3-3v-7h5c0.552,0,1-0.447,1-1v-8   c0-0.553-0.448-1-1-1h-5v-4h12V61z M60,35H35V15h7c10.477,0,19,8.523,19,19C61,34.552,60.551,35,60,35z"/>
                                <rect x="45" y="23" width="10" height="2"/>
                                <rect x="45" y="27" width="10" height="2"/>
                                <rect x="45" y="31" width="10" height="2"/>
                                <path d="M22,1H2C1.448,1,1,1.448,1,2v42c0,0.553,0.448,1,1,1h20c0.552,0,1-0.447,1-1V2C23,1.448,22.552,1,22,1z M21,43H3V3h18V43z"/>
                                <rect x="5" y="29" width="14" height="2"/>
                                <rect x="5" y="39" width="14" height="2"/>
                                <rect x="5" y="25" width="14" height="2"/>
                                <rect x="5" y="15" width="14" height="2"/>
                                <rect x="5" y="5" width="14" height="2"/>
                                <rect x="5" y="33" width="14" height="4"/>
                                <rect x="5" y="19" width="14" height="4"/>
                                <rect x="5" y="9" width="14" height="4"/>
                            </g>
                            </svg>
                    </div>
                    <div class="w-full">
                        <button class="mt-6 pt-2 pl-4 pr-4 pb-2 rounded-md text-white bg-blue-800 w-full hidden" onclick="activateFocus()" id="btnfocus">Activar</button>
                    </div>
                    <form action="#" id="frm_meet_ap_es" autocomplete="off">
                        <input type="text" id="inpqrcode" style="width:0;" onfocus="activateBlur(true)"  onfocusout="activateBlur(false)" autofocus="true">
                    </form>
                </div>
                <div class="w-full md:w-1/2">

                        <div class="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden md:max-w-md ">
                            <div class="md:flex">
                                <div class="w-full p-2 py-10 text-center">
                                    <x-roller-loader class="my-8" style="display: none !important;"></x-roller-loader>
                                    <div id="personinfo">
                                        <div class="flex justify-center">
                                            <div class="relative"> <img src="{{ $picture }}" class="rounded-full" width="100" id="img_user_selected"></div>
                                        </div>
                                        <div class="flex flex-col text-center mt-3 mb-4"> <span class="text-2xl font-medium">{{ $full_name }}</span> <span class="text-md text-gray-400">{{$area}}</span> </div>
                                        {{-- <p class="px-16 text-center text-md text-gray-800">Actress, musician, songwriter, and artist.DM for work inquires or <a class="text-blue-800 text-md font-bold" href="#">#tag </a>me in your message.</p> --}}
                                        <div class="px-16 mt-3 text-center">
                                            {{-- <span class="bg-gray-100 h-5 p-1 px-3 rounded cursor-pointer hover:shadow hover:bg-gray-200" id="spn_state"></span>  --}}
                                            @if (strlen( $full_name ) > 1)
                                            <span class="bg-gray-100 h-5 p-1 px-3 rounded cursor-pointer hover:shadow hover:bg-gray-200">#{{$dni}}</span>
                                            @else
                                            <span class="bg-gray-100 h-5 p-1 px-3 rounded cursor-pointer hover:shadow hover:bg-gray-200">No seleccionado</span>
                                            @endif

                                            
                                            {{-- <span class="bg-gray-100 h-5 p-1 px-3 rounded cursor-pointer hover:shadow hover:bg-gray-200" id="spn_email"></span>  --}}
                                        </div>
                                        <div class="px-14 mt-5">
                                            @if (strlen( $full_name ) > 1)
                                                <button class="h-12 bg-gray-200 w-full text-black text-md rounded hover:shadow hover:bg-gray-300 mb-2" wire:click="clearProps()" onclick="activateFocus()">{{__('Clear')}}</button>
                                                @if ($Meet->request_temp == 1)
                                                    <button class="h-12 bg-blue-700 w-full text-white text-md rounded hover:shadow hover:bg-blue-800" onclick="mistoreMeet()">{{__('Enter')}}</button>
                                                    <button id="btncustonstoremeet" class="hidden" wire:click="storeMeetParticipant()" >ok</button>
                                                    @else
                                                    <button class="h-12 bg-blue-700 w-full text-white text-md rounded hover:shadow hover:bg-blue-800" wire:click="storeMeetParticipant()" onclick="activateFocus()">{{__('Enter')}}</button>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <button class="hidden" id="close_my_meet" wire:click="closeMeet()" >0</button>
                </div>
            </div>
            <div class="overflow-hidden pb-2  w-full">

                <x-table class="w-full">
                    <x-slot name="thead">
                        <tr>
                            <x-th-standar>{{__('SAP_Code')}}</x-th-standar>
                            <x-th-standar>{{__('Full name')}}</x-th-standar>
                            <x-th-standar>{{__('Dni')}}</x-th-standar>
                            <x-th-standar>{{__('Area')}}</x-th-standar>
                            @if ($Meet->request_temp == 1)
                                <x-th-standar>{{__('Temperature')}}</x-th-standar>
                            @endif
                            <x-th-standar>{{__('Firm')}}</x-th-standar>
                            </tr>
                    </x-slot>
                    <x-slot name="tbody">
                            @forelse ($participants as $participant)
                            @php
                                $temp = "Guardada";
                                if(isset($participant->pivot->temp)){
                                    $temp = $participant->pivot->temp;
                                }
                            @endphp
                                    <tr>
                                        <x-table-meet-participants
                                        sapcode="{{ $participant->code }}"
                                        fullname="{{ $participant->full_name }}"
                                        dni="{{ $participant->dni }}"
                                        area="{{ $participant->area }}"
                                        temp="{{$temp}}"
                                        firm="{{ $participant->firm }}"
                                        gettemp="{{$Meet->request_temp}}"
                                        ></x-table-meet-participants>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="5">
                                            <p class="font-semibold my-4 ">No hay registros </p>
                                        </td>
                                    </tr>
                                @endforelse
                    </x-slot>
                </x-table>
                {{$participants->links()}}
            </div>
        </div>
    </div>


      <script>
            let doc = document.getElementById('inpqrcode')
            let loader = document.getElementById('rollerloader')
            let peri = document.getElementById('personinfo')
            doc.value=""

            function activateBlur(dess){
                let lectorshowed = document.getElementById('Layer_3')
                let btnf = document.getElementById('btnfocus')
                if(!dess){
                    lectorshowed.classList.add('bg-red-600')
                    lectorshowed.classList.remove('bg-green-600')
                    btnf.classList.remove('hidden')
                }else{
                    lectorshowed.classList.remove('bg-red-600')
                    lectorshowed.classList.add('bg-green-600')
                    btnf.classList.add('hidden')
                }

        }

        function activateFocus(){
            doc.focus()
        }

        frm_meet_ap_es.addEventListener('submit',function(e){
            e.preventDefault()
            peri.style.display = "none"
            loading("inline-block")
            let one = "9a6400931a25f06c0e9a24022c246a10e69b5281"
            let two = "49837fd091472134706031790a00afb6"
            let three = "112545315524"

            if(doc.value != ""){

                localParticipant(doc.value)
                .then(r=>JSON.parse(r))
                .then((existeLocal)=>{                    
                    // console.log("resultado de la promesa ====> ",existeLocal)
                    if(!existeLocal.status){
                        fetch('https://espumred.com/index.php/Informacionempleado/apigetempleado/'+existeLocal.dni+'?a-t='+one+'&a-k='+two+'&u-k='+three).then(response=>response.json())
                        .then(emplo=>{

                            if(emplo[0] != null){
                                @this.code =  emplo[0].codigoNomina
                                @this.full_name = emplo[2].nombre + " " + emplo[2].apellido
                                @this.dni = emplo[2].cc
                                @this.area = "@"+emplo[1].nombreArea
                                @this.firm = emplo[2].nombre + " " + emplo[2].apellido
                                @this.picture = emplo[0].foto

                            }else{
                                reloadModelFi()
                                let dess = confirm("El usuario no se encuentra registrado en espumred ¿ desea agregarlo como temporal ?")
                                if(dess){
                                    // alert("Mostrar ventana para scannear cedula");
                                    window.location.href= "/participant/create/"+existeLocal.meet
                                }
                            }
                            loading("none !important")
                            peri.style.display = "block !important"
                        }).catch(function(er){
                            loading("none")
                            peri.style.display = "block"
                            reloadModelFi()
                            alert("Yiyi Internet fail :(")
                        })
                    }
                })
                doc.value=""
            }else{
                alert("No se ha leido el código Qr o no se ha ingresado un documento.")
                loading("none")
                reloadModelFi()
            }
        })

        function closeMeet(){
            if(confirm("Desea finalizar la reunión?")){
                document.getElementById('close_my_meet').click()
            }
        }

        function loading(opt = "none !important"){
            loader.style.display = opt
        }

        function reloadModelFi(){
            @this.code = ""
            @this.full_name = ""
            @this.dni = ""
            @this.area = ""
            @this.firm = ""
            @this.temp = ""
            @this.picture = "{{ asset('img/users/default.png') }}"
        }

        function localParticipant(dni){
           return @this.checkParticipant(dni)
        }

        function mistoreMeet(){
            let temp = ""

            while(temp == ""){
                temp = prompt("Ingrese la temperatura del participante")
            }

            let btn = document.getElementById('btncustonstoremeet')
            @this.temp = temp
            btn.click()
            activateFocus()
        }
    </script>




