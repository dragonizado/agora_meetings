
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create new meetings') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden pb-2">
                <x-jet-form-section submit="#">
                    <x-slot name="title">
                        {{ __('Meetings Details') }}
                    </x-slot>

                    <x-slot name="description">
                        {{ __('Create a new meetings to register participants') }}
                    </x-slot>

                    <x-slot name="form">

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="type" value="{{ __('Type') }}" />
                            <select name="" id=""  class="block appearance-none w-full border shadow-sm border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" wire:model="type" required>
                                <option value=""  selected>--</option>
                                <option value="Formación">Formación</option>
                                <option value="Divulgación">Divulgación</option>
                                <option value="Reunión">Reunión</option>
                                <option value="Otros">Otros</option>
                            </select>
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="topic" value="{{ __('Topic') }}" />
                            <x-jet-input id="topic" type="text" class="mt-1 block w-full" wire:model="topic"/>
                            <x-jet-input-error for="topic" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="objective" value="{{ __('Objective') }}" />
                            <x-jet-input id="objective" type="text" class="mt-1 block w-full" wire:model="objective" />
                            <x-jet-input-error for="objective" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="duration" value="{{ __('Duration') }}" />
                            <x-jet-input id="numduration" type="text" class="mt-1 w-1/3" wire:model="numduration" />
                            <select name="typeduration" id="typeduration"  class=" appearance-none w-1/2 border shadow-sm border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" wire:model="typeduration">
                                <option value="" selected>--</option>
                                <option value="Horas">Horas</option>
                                <option value="Minutos">Minutos</option>
                            </select>
                        </div>

                           <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="area" value="{{ __('Area - Sub process') }}" />
                            {{-- <x-jet-input id="area" type="text" class="mt-1 block w-full" wire:model="area" /> --}}
                            <select name="area" id="area"  class=" appearance-none w-full border shadow-sm border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" wire:model="area">
                                <option value="">Seleccione un area</option>
                                @foreach ($agora_code_areas as $area)
                                    <option value="{{ $area->code }}">{{ $area->name }}</option>
                                @endforeach
                            </select>
                            <x-jet-input-error for="area" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="facilitator_name" value="{{ __('Facilitator name') }}" />
                            <x-jet-input id="facilitator_name" type="text" class="mt-1 block w-full" wire:model="facilitatorsname" />
                            <x-jet-input-error for="facilitator_name" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="agora_code" value="{{ __('Agora code') }}" />
                            <x-jet-input id="agora_code" type="text" class="mt-1 block w-full" wire:model="agora_code" />
                            <x-jet-input-error for="agora_code" class="mt-2" />
                        </div>

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="tags" value="{{ __('Tag') }}" />
                            <x-jet-input id="tags" type="text" class="mt-1 block w-full" wire:model="tag" />
                            <x-jet-input-error for="tag" class="mt-2" />
                        </div>
                        {{-- <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="agora_code" value="{{ __('Request temperature') }}" />
                            <select name="" id=""  class="block appearance-none w-full border shadow-sm border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" wire:model="request_temp">
                                <option value="0" selected>No</option>
                                <option value="1">SI</option>
                            </select>
                        </div> --}}

                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="observations" value="{{ __('Observations') }}" />
                            <textarea name="observations" id="observations" class="form-input rounded-md shadow-sm mt-1 block w-full" cols="30" rows="10" wire:model="observations"></textarea>
                            <x-jet-input-error for="observations" class="mt-2" />
                        </div>

                         
                    </x-slot>

                    <x-slot name="actions">
                        <x-jet-button type="button" wire:click="store()">
                            {{ __('Create') }}
                        </x-jet-button>
                    </x-slot>
                </x-jet-form-section>
            </div>
        </div>
    </div>
