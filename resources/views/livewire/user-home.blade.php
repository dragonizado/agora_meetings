<x-table>
    <x-slot name="thead">
        <tr>
              <x-th-standar>Nombre Organizador</x-th-standar>
              <x-th-standar>Tema</x-th-standar>
              <x-th-standar>Estado</x-th-standar>
              <x-th-standar>Consecutivo Ágora</x-th-standar>
              <x-th-basic><span class="sr-only">Edit</span></x-th-basic>
            </tr>
    </x-slot>
    <x-slot name="tbody">
            @forelse ($meetings as $meet)
               @php
                    $permission = false;

                    if($meet->user->id == Auth()->user()->id){
                        $permission = true;
                    }
                @endphp
                     <tr>
                        <x-table-meet-info 
                        identificator="{{ $meet->id }}"
                        picture="{{ $meet->user->profile_photo_url }}"
                        name="{{ $meet->user->name }} " 
                        email="{{ $meet->user->email }}" 
                        tema="{{ $meet->topic }}" 
                        objetive="{{ $meet->objective }}" 
                        state="{{ $meet->state }}" 
                        permission="{{ $permission }}" 
                        agoracod="{{ $meet->agora_code }}"></x-table-meet-info>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="5">
                            <p class="font-semibold my-4 ">No hay registros </p>
                        </td>
                    </tr>
                @endforelse
    </x-slot>
</x-table>