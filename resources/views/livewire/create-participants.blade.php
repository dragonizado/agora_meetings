<div>
<x-slot name="header">
    <div class="flex">
        <div class="w-full">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{__("Registe participant on ")}} - {{ $Meet->topic }}
                </h2>
                <div class="text-sm text-gray-500">{{ $Meet->objective }}</div>
        </div>
        
    </div>
</x-slot>

 <form action="#" id="frmscanparticipant" autocomplete="off">
        <input type="text" id="qrdni" onfocus="activateBlur(true)" 
        onfocusout="activateBlur(false)" autofocus="true" style="width:0; heigth:0;">
        <input type="text" id="qrlastnameone" style="width:0; heigth:0;">
        <input type="text" id="qrlastnametwo" style="width:0; heigth:0;">
        <input type="text" id="qrfirstnameone" style="width:0; heigth:0;">
        <input type="text" id="qrfirstnametwo" style="width:0; heigth:0;">
        <input type="text" id="qrsex" style="width:0; heigth:0;">
        <input type="text" id="qrdate" style="width:0; heigth:0;">
        <input type="text" id="qrrh" onfocus="activateBlur(true)" style="width:0; heigth:0;">
        <button type="submit" style="width:0; heigth:0;"></button>
</form>

   <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden pb-2 flex flex-wrap">
                <div class="w-full md:w-1/2 ">

                    <div class="w-full flex justify-center">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_3" x="0px" y="0px" viewBox="0 0 64 64" 
                            id="Layer_3" 
                            style="enable-background:new 0 0 64 64;" xml:space="preserve" width="300" height="300"
                            class="mt-4 bg-green-600 p-8 rounded-lg">
                                <g>
                                    <path d="M58,13H6c-2.757,0-5,2.243-5,5v2v4v22c0,2.757,2.243,5,5,5h52c2.757,0,5-2.243,5-5V24v-4v-2C63,15.243,60.757,13,58,13z    M6,15h52c1.654,0,3,1.346,3,3v1H3v-1C3,16.346,4.346,15,6,15z M61,23H3v-2h58V23z M58,49H6c-1.654,0-3-1.346-3-3V25h58v21   C61,47.654,59.654,49,58,49z"/>
                                    <path d="M58,28H42c-0.553,0-1,0.448-1,1v16c0,0.552,0.447,1,1,1h16c0.553,0,1-0.448,1-1V29C59,28.448,58.553,28,58,28z M57,44H43   V30h14V44z"/>
                                    <rect x="45" y="32" width="2" height="2"/>
                                    <rect x="45" y="40" width="2" height="2"/>
                                    <path d="M51,37v-5h-2v4h-4v2h5C50.553,38,51,37.552,51,37z"/>
                                    <rect x="53" y="32" width="2" height="2"/>
                                    <path d="M49,42h5c0.553,0,1-0.448,1-1v-5h-2v4h-4V42z"/>
                                    <path d="M17,35c-1.093,0-2.116,0.299-3,0.812C13.116,35.299,12.093,35,11,35c-3.309,0-6,2.691-6,6s2.691,6,6,6   c1.093,0,2.116-0.299,3-0.812C14.884,46.701,15.907,47,17,47c3.309,0,6-2.691,6-6S20.309,35,17,35z M14,43.618   c-0.615-0.703-1-1.612-1-2.618s0.385-1.914,1-2.618c0.615,0.703,1,1.612,1,2.618S14.615,42.914,14,43.618z M7,41   c0-2.206,1.794-4,4-4c0.468,0,0.91,0.096,1.328,0.244C11.499,38.273,11,39.579,11,41s0.499,2.727,1.328,3.756   C11.91,44.904,11.468,45,11,45C8.794,45,7,43.206,7,41z M17,45c-0.468,0-0.91-0.096-1.328-0.244C16.501,43.727,17,42.421,17,41   s-0.499-2.727-1.328-3.756C16.09,37.096,16.532,37,17,37c2.206,0,4,1.794,4,4S19.206,45,17,45z"/>
                                    <rect x="5" y="31" width="18" height="2"/>
                                </g>
                            </svg>
                    </div>
                    <div class="w-full">
                        <button class="mt-6 pt-2 pl-4 pr-4 pb-2 rounded-md text-white bg-blue-800 w-full hidden" onclick="activateFocus()" id="btnfocus">Activar</button>
                    </div>
                    
                </div>
                <div class="w-full md:w-1/2">
{{-- ------------------------------------------------ --}}

<div class="mt-5 md:mt-3 md:col-span-2">
      
        <div class="shadow overflow-hidden sm:rounded-md">
          <div class="px-4 py-5 bg-white sm:p-6">
            <div class="grid grid-cols-6 gap-6">
              <div class="col-span-6 sm:col-span-3">
                <label for="first_name" class="block text-sm font-medium text-gray-700">{{__("First name")}}</label>
                <input type="text"  id="first_name" autocomplete="given-name"  class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" wire:model="firstName">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="last_name" class="block text-sm font-medium text-gray-700">{{__("Last name")}}</label>
                <input type="text" id="last_name"   class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" wire:model="lastName">
              </div>

              <div class="col-span-6 sm:col-span-4">
                <label for="email_address" class="block text-sm font-medium text-gray-700">{{__("Document")}}</label>
                <input type="text" id="email_address"   class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" wire:model="dni">
              </div>
              @if ($dni != "")    
                <div class="col-span-6 sm:col-span-3">
                    <label for="country" class="block text-sm font-medium text-gray-700">{{ __("Area") }}</label>
                    <select id="country" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" wire:model="area">
                        <option value="Empleado - Sin registro">Empleado - Sin registro</option>
                    <option value="Proveedor">Proveedor</option>
                    <option value="Visitante">Contratistas</option>
                    <option value="Visitante">Visitante</option>
                    </select>
                </div>
              @endif

              @if ($Meet->request_temp == 1)
                  <div class="col-span-6">
                    <label for="street_address" class="block text-sm font-medium text-gray-700">{{ __("Temp") }}</label>
                    <input type="text" id="street_address" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" wire:model="temp">
                </div>
              @endif

            </div>
          </div>
          <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
            <button  class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onclick="register()">
              {{__("Create")}}
            </button>
          </div>
        </div>
      
    </div>

{{-- ------------------------------------------------ --}}
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    let doc = document.getElementById("qrdni")
    let form = document.getElementById("frmscanparticipant")

    form.reset();

    form.addEventListener('submit',function(e){
        e.preventDefault()
        let fn = `${frmscanparticipant.qrfirstnameone.value} ${frmscanparticipant.qrfirstnametwo.value} ${frmscanparticipant.qrlastnameone.value} ${frmscanparticipant.qrlastnametwo.value}`
        @this.code = ""
        @this.fullName = fn
        @this.dni = frmscanparticipant.qrdni.value
        // @this.area = "@TEMPORAL"
        @this.firm = fn
        @this.firstName = `${frmscanparticipant.qrfirstnameone.value} ${frmscanparticipant.qrfirstnametwo.value}`
        @this.lastName = `${frmscanparticipant.qrlastnameone.value} ${frmscanparticipant.qrlastnametwo.value}`

        // resetForm()
    })

    function activateBlur(dess,bn){
                let lectorshowed = document.getElementById('Layer_3')
                let btnf = document.getElementById('btnfocus')
                if(!dess){
                    lectorshowed.classList.add('bg-red-600')
                    lectorshowed.classList.remove('bg-green-600')
                    btnf.classList.remove('hidden')
                }else{
                    lectorshowed.classList.remove('bg-red-600')
                    lectorshowed.classList.add('bg-green-600')
                    btnf.classList.add('hidden')
                }

        }

    function register(){
        resetForm()
        @this.store()
    }

    function activateFocus(){
            doc.focus()
    }

    function resetForm(){
        form.reset();
    }

    function resetComponent(){
        @this.code = ""
        @this.fullName = ""
        @this.dni = ""
        // @this.area = ""
        @this.firm = ""
        @this.firstName = ""
        @this.lastName = ""
    }

</script>
