<td class="px-6 py-4 whitespace-nowrap">
    <div class="text-sm text-gray-900"> {{$sapcode}}</div>
</td>

<td class="px-6 py-4 whitespace-nowrap">
    <div class="text-sm text-gray-900"> {{$fullname}}</div>
</td>
<td class="px-6 py-4 whitespace-nowrap">
    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                   {{$dni}}
    </span>
</td>
<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
     {{$area}}
</td>
@if ($gettemp)
    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
        {{$temp}}
    </td>
@endif
<td class="px-6 py-4 whitespace-nowrap text-left text-sm font-medium">
    {{$firm}}
</td>