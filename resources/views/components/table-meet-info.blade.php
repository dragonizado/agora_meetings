<td {{ $attributes->merge(['class'=>'px-6 py-4 whitespace-nowrap'])}}>
                <div class="flex items-center">
                  <div class="flex-shrink-0 h-10 w-10">
                  <img class="h-10 w-10 rounded-full" src="{{ $picture }}" alt="{{$name}}">
                  </div>
                  <div class="ml-4">
                    <div class="text-sm font-medium text-gray-900">
                      {{$name}}
                    </div>
                    <div class="text-sm text-gray-500">
                       {{$email}}
                    </div>
                  </div>
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
              <div class="text-sm text-gray-900"> <a href="{{ route('meet.show',$identificator) }}">{{$tema}}</a></div>
                <div class="text-sm text-gray-500"> {{$objetive}}</div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                   {{$state}}
                </span>
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                 {{$agoracod}}
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                @if ($permission)
                    
                  @if ($state == App\Models\Meet::pendiente)
                    <a href="{{ route('meet.update', $identificator) }}" class="text-indigo-600 hover:text-indigo-900">Iniciar</a>
                  @endif

                  @if($state == App\Models\Meet::en_progreso)
                      <a href="{{ route('meet.update', $identificator) }}" onclick="return confirm('Deseas continuar esta con reunión?')" class="text-indigo-600 hover:text-indigo-900">Continuar</a>
                  @endif

                  @if($state == App\Models\Meet::terminada)
                      <a href="{{ route('meet.show', $identificator) }}" class="text-indigo-600 hover:text-indigo-900">Ver</a>
                  @endif
                  @else
                  <a href="{{ route('meet.show', $identificator) }}" class="text-indigo-600 hover:text-indigo-900">Ver</a>
                @endif
</td>
