<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Area;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            [
                "code"=>'0001',
                "name"=>"SST"
            ],
            [
                "code"=>'0002',
                "name"=>"Gestión humana"
            ],
            [
                "code"=>'0003',
                "name"=>"Calidad "
            ],
            [
                "code"=>'0004',
                "name"=>"Sistemas"
            ],
            [
                "code"=>'0005',
                "name"=>"Comercial "
            ],
            [
                "code"=>'0006',
                "name"=>"Servicio al cliente "
            ],
            [
                "code"=>'0007',
                "name"=>"Ingeniería"
            ],
            [
                "code"=>'0008',
                "name"=>"GE logistica "
            ],
            [
                "code"=>'0009',
                "name"=>"Compras "
            ],
            [
                "code"=>'0010',
                "name"=>"Contabilidad "
            ],
            [
                "code"=>'0011',
                "name"=>"Mercadeo"
            ],
            [
                "code"=>'0012',
                "name"=>"Auditoría"
            ],
            [
                "code"=>'0013',
                "name"=>"Gestión documental "
            ],
            [
                "code"=>'0014',
                "name"=>"Mantenimiento "
            ],
            [
                "code"=>'0015',
                "name"=>"Gerencia administrativa "
            ],
            [
                "code"=>'0016',
                "name"=>"Cartera "
            ],
            [
                "code"=>'0017',
                "name"=>"Producción "
            ],
            [
                "code"=>'0018',
                "name"=>"Diseño"
            ],
            [
                "code"=>'0019',
                "name"=>"copasst"
            ],
            [
                "code"=>'0020',
                "name"=>"comite de convivencia"
            ],
        ];


        foreach ($areas as $area) {
            $search = $this->searchArea($area['name']);
            if(!$search->exist){
                $search->fill(["code"=>$area['code'],"name"=>$area['name']])->save();
            }
        }



        
    }

    protected function searchArea($name){
        return Area::firstOrNew(["name"=>$name]);
    }
}
