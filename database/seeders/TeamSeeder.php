<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Team;
class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agora = new Team();

        $agora->user_id = 1;
        $agora->name = "Agora";
        $agora->personal_team = 0;

        $agora->save();
    }
}
