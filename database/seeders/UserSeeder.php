<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::firstOrNew(["name"=>"Administrador"]);

        if(!$admin->exists){
            $admin->fill([
                "email"=>"admin@agora.com",
                "password"=>Hash::make("123456789"),
                "current_team_id"=>1
            ])->save();
        }
        
    }
}
