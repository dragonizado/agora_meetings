<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meet_participants', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('participants_id');
            $table->unsignedBigInteger('meet_id');
            $table->string('temp')->nullable();
            $table->foreign('participants_id')->references('id')->on('participants')->onDelete('cascade');
            $table->foreign('meet_id')->references('id')->on('meetings')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meet_participants');
    }
}
