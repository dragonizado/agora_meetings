<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAreatypedurationfaciToMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->string("area");
            $table->string("type");
            $table->string("num_duration");
            $table->string("type_duration");
            $table->string("facilitators_name");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->dropColumn("area");
            $table->dropColumn("type");
            $table->dropColumn("num_duration");
            $table->dropColumn("type_duration");
            $table->dropColumn("facilitators_name");
        });
    }
}
