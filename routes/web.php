<?php

use Illuminate\Support\Facades\Route;



use App\Http\Controllers\SiteController;
use App\Http\Controllers\MeetController;

use App\Http\Livewire\CreateMeetings;
use App\Http\Livewire\UpdateMeetings;
use App\Http\Livewire\ShowMeetings;
use App\Http\Livewire\UpdateParticipants;
use App\Http\Livewire\CreateParticipants;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [SiteController::class,'index'])->name('home');
Route::group(['middleware'=>['auth:sanctum','verified']],function(){
    Route::get('/dashboard',[SiteController::class,'dashboard'])->name('dashboard');

    Route::group(['prefix'=>'meet'],function(){
        Route::get('/create',CreateMeetings::class)->name('meet.create');
        Route::get("/export",[MeetController::class,"export"])->name("meet.export");
        Route::post("/export",[MeetController::class,"exportExcel"])->name("meet.exportexcel");
        Route::get('/show/{meet}', ShowMeetings::class)->name('meet.show');
        Route::get('/update/{Meet}',UpdateMeetings::class)->name('meet.update');
    });

    Route::group(['prefix'=>'participant'],function(){
        Route::get('/',UpdateParticipants::class)->name('participants.index');

        Route::get('/create/{Meet}',CreateParticipants::class)->name('participants.create');
    });
});
