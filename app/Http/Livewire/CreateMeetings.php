<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Meet;
use App\Models\Area;

class CreateMeetings extends Component
{
    public $topic, 
    $objective,
    $state,
    $observations,
    $request_temp,
    $tag,
    $facilitatorsname,
    $area,
    $type,
    $numduration,
    $typeduration,
    $agora_code_areas,
    $agora_code;

    protected $rules = [
        'typeduration' => 'required',
        'tag' => 'required',
        'facilitatorsname' => 'required',
        'numduration' => 'required',
        'agora_code' => 'required',
        'type' => 'required',
        'topic' => 'required',
        'objective' => 'required',
        'area' => 'required',
    ];

    public function mount(){
        $this->agora_code_areas = Area::all();
        $this->agora_code = (Meet::where("user_id",Auth::user()->id)->count()) + 1;
    }

    public function render()
    {
        return view('livewire.create-meetings');
    }

    public function store(){
        $this->validate();
        Meet::create([
            'topic' => $this->topic,
            'objective' =>$this->objective,
            'user_id' => Auth::user()->id,
            'state' => Meet::pendiente,
            'observations' => $this->observations,
            'tag' => $this->tag,
            'request_temp'=>0,
            'area'=>$this->area,
            'type'=>$this->type,
            'num_duration'=>$this->numduration,
            'type_duration'=>$this->typeduration,
            'facilitators_name'=>$this->facilitatorsname,
            'agora_code' => $this->agora_code,
        ]);

        return redirect()->route('dashboard');
    }
}
