<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Participants;

class UpdateParticipants extends Component
{
    public $participant,$full_name,$dni,$area,$code;


    public function mount(Participants $Participant){
        $this->Participant = $Participant;
        $this->full_name = $Participant->full_name;
        $this->dni = $Participant->dni;
        $this->area = $Participant->area;
        $this->code = $Participant->code;
    }

    public function render()
    {
        return view('livewire.update-participants');
    }

    public function update(){
        $participant = $this->participant;
        $participant->full_name = $this->full_name;
        $participant->dni = $this->dni;
        $participant->area = $this->area;
        $participant->code = $this->code;
        $participant->save();
    }
}
