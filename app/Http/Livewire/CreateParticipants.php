<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Meet;
use App\Models\Participants;

class CreateParticipants extends Component
{
    public $Meet;

    public $code = "";
    public $fullName;
    public $dni;
    public $area = "Empleado - Sin registro";
    public $firm;
    public $temp;
    public $firstName;
    public $lastName;

    public function mount(Meet $Meet){
        $this->Meet = $Meet;
    }
    public function render()
    {
        return view('livewire.create-participants');
    }

    public function store(){
        if(!$this->checkParticipant()){
            $model = new Participants();
            $model->code = $this->code;
            $model->full_name = strtoupper($this->firstName ." ".$this->lastName);
            $model->dni = $this->cleanDni();
            $model->area = "@".strtoupper($this->area);
            $model->firm = ucwords($this->firstName ." ".$this->lastName);

            if($model->save()){

                if($this->Meet->request_temp){
                    $model->meets()->attach($this->Meet->id,['temp'=>  (String)$this->temp]);
                }else{
                    $model->meets()->attach($this->Meet->id);
                }

            }

            return redirect()->route('meet.update',$this->Meet->id);
        }else{

        }

    }

    private function cleanDni(){
        $dni = $this->dni;
        $band = true;
        $newDni = null;
        for ($i=0; $i < 10; $i++) {
            if($band){
                if((Integer)$dni[$i] != 0){
                    $band = false;
                    if(is_null($newDni)){
                        $newDni = $dni[$i];
                    }else{
                        $newDni .= $dni[$i];
                    }
                }
            }else{
                if(is_null($newDni)){
                    $newDni = $dni[$i];
                }else{
                    $newDni .= $dni[$i];
                }
            }
            
        }
        return $newDni;
    }

    private function checkParticipant(){
        $participant = Participants::where('dni',$this->dni)->first();

        if($participant){
            return true;
        }else{
            return false;
        }
    }
}
