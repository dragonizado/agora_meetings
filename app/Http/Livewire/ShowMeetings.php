<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Meet;

class ShowMeetings extends Component
{
    public $Meet;
    public $search;
    public function mount(Meet $meet){
        $this->Meet = $meet;
    }
    public function render()
    {

        $participants = $this->Meet->participants();
        $participants->where("full_name","LIKE","%{$this->search}%");

        $participants = $participants->paginate(15);
        return view('livewire.show-meetings',['participants'=> $participants]);
    }
}
