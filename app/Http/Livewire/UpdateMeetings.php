<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;
use App\Models\Meet;
use App\Models\Participants;
use Livewire\WithPagination;

class UpdateMeetings extends Component
{
    use WithPagination;

    public
    $Meet,
    $code,
    $full_name,
    $dni,
    $area,
    $picture,
    $temp,
    $firm;

    public $loadedLocalEmployer = false;

    protected $listeners = ["checkParticipant"];

    // public $participants = [];

    public function mount(Meet $Meet){
        $this->Meet = $Meet;
        // $this->picture = "https://i.imgur.com/z4YSzDD.jpg";
        $this->picture = asset('img/users/default.png');

        if($Meet->state == Meet::pendiente){
            $Meet->state =  Meet::en_progreso;
             $Meet->start_date = today();
            $Meet->save();
        }

        // dd($Meet->participants()->orderBy('created_at','DESC')->paginate(2));

        // $this->participants = $Meet->participants()->orderBy('created_at','DESC')->paginate(5);
    }

    public function checkParticipant($dni){
        $participant = Participants::where('dni',$dni)->first();
        if($participant){
            // dd($participant);
            $this->code = $participant->code;
            $this->full_name = $participant->full_name;
            $this->dni = $participant->dni;
            $this->area = $participant->area;
            $this->loadedLocalEmployer = true;
            return json_encode(["status"=>true,"dni"=>$dni,"meet"=>$this->Meet->id]);
        }else{
            $this->loadedLocalEmployer = false;
            return json_encode(["status"=>false,"dni"=>$dni,"meet"=>$this->Meet->id]);
        }
    }

    public function createParticipant(){
        dd("yeyeye");
        return view("welcome");
    }

    public function render()
    {
        $Meet = $this->Meet;

        return view('livewire.update-meetings',['participants'=>$Meet->participants()->orderBy('created_at','DESC')->paginate(15)]);
    }


    public function clearProps(){
       $this->picture = asset('img/users/default.png');
       $this->code = "";
       $this->full_name = "";
       $this->dni = "";
       $this->area = "";
    //    $this->temp = "";
       $this->firm = "";
    }

    public function closeMeet(){
        if(count($this->Meet->participants()->get()) > 0){
            if($this->Meet->state == Meet::en_progreso && $this->Meet->state != Meet::pendiente ){
                $this->Meet->state = Meet::terminada;
                $this->Meet->end_date = today();
                if($this->Meet->save()){
                    return redirect()->route('dashboard');
                }
            }
        }else{
            $this->openModal("Error", "No se puede finalizar la reunión sin participantes");
        }
    }

    public function storeMeetParticipant(){
        $participant = Participants::where('dni', $this->dni)->first();
        if(!isset($participant->dni)){
            $participant = new Participants();
            $participant->code = $this->code;
            $participant->full_name = $this->full_name;
            $participant->dni = $this->dni;
            $participant->area = $this->area;
            $participant->firm = $this->firm;
            $participant->save();
        }

        if(!$this->isFirm($participant)){
            if($this->Meet->request_temp){
                $participant->meets()->attach($this->Meet->id,['temp'=>  (String)$this->temp]);
            }else{
                $participant->meets()->attach($this->Meet->id);
            }
            $this->clearProps();
            // $this->participants = $this->Meet->participants()->orderBy('created_at','DESC')->get();
        }else{
            abort(400,"El asistente ya ha firmado en esta reunión");
        }
    }

    // public function openModal($title, $text){
    //     $this->mensajeModal($title, $text);
    //     $this->openModal = true;
    // }

    // private function mensajeModal($title="",$text = ""){
    //     $this->modal = [
    //         "title" => $title,
    //         "body" => $text,
    //     ];
    // }

    // public function closeModal(){
    //     $this->mensajeModal();
    //     $this->openModal = false;
    // }



    private function isFirm($participant){
        return DB::table('meet_participants')
        ->where('participants_id',$participant->id)
        ->where('meet_id',$this->Meet->id)
        ->first();
    }


}
