<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Meet;
use App\Models\Participants;


use App\Exports\MeetsExport;
use Maatwebsite\Excel\Facades\Excel;


class MeetController extends Controller
{
    public function create(Request $Request){
        return view('meet.create');
    }

    public function export(){
        $users = User::all();
        return view("meet.exportdata",compact('users'));
    }

    public function exportExcel(Request $request){
        if($request->has("usersid")){
            $users = $request->input("usersid");
            $datein = $request->input("datein");
            $dateout = $request->input("dateout");
            return Excel::download(new MeetsExport($users ,$datein,$dateout), 'Reuniones_'.$datein.'_.xlsx');
        }
    }
}
