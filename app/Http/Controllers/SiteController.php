<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meet;

class SiteController extends Controller
{
    public function index(Request $Request){
        return view('welcome');
    }

    public function dashboard(Request $Request){        
        return view('dashboard');
    }
}
