<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Auth;

use App\Models\Meet;
use App\Models\Team;

class UserHome extends Component
{
    public function render()
    {
        $team = Auth::user()->currentTeam;

        if(Auth::user()->hasTeamPermission($team,'read')){
            $meetings = Meet::orderBy('created_at','DESC')
            ->get();
        }else{
            $meetings = Meet::where('user_id',Auth::user()->id)
            ->orderBy('created_at','DESC')
            ->get();         
        }

        return view('livewire.user-home',compact('meetings'));
    }
}
