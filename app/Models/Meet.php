<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meet extends Model
{
    protected $table= 'meetings';

    protected $fillable = ['topic',
'objective',
'user_id',
'state',
'observations',
'request_temp',
'agora_code',
'area',
'type',
'num_duration',
'type_duration',
'facilitators_name',
'tag'
];

    const pendiente = 'pendiente';
    const en_progreso = 'en_progreso';
    const terminada = 'terminada';
    
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function participants(){
        return $this->belongsToMany(Participants::class)->withPivot('temp')->withTimestamps();
    }
}
