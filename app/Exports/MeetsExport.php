<?php

namespace App\Exports;

use App\Models\Meet;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MeetsExport implements FromView
{
    protected $users;
    protected $datein;
    protected $dateout;
    protected $apiEmployes;

    public function __construct($users,$datein = "0000-00-00 000000",$dateout = "0000-00-00 000000"){

        $this->users = $users;
        $this->datein = $datein;
        $this->dateout = $dateout;

        $apiBaseUrl = "https://espumred.com/index.php/Informacionempleado/ApiGetAllEmployes";
        $apiBaseUrl .= "";
        $apiBaseUrl .= "";
        $apiBaseUrl .= "";

        $this->apiEmployes = json_decode(\file_get_contents($apiBaseUrl));
    }

    public function view(): View{
        $meets = Meet::select(["id","topic","objective","state","observations","request_temp","created_at"])
        ->where("created_at",">=",$this->datein)
        ->where("created_at","<=",$this->dateout)
        ->where("state","=",Meet::terminada)
        ->whereIn('user_id',$this->users)
        ->get();
        return view("meet.excel",["meets"=>$meets,"apiemployes"=>$this->apiEmployes]);
    }

    // public function map($meet): array
    // {
    //     return [
    //         $meet->id,
    //         $meet->topic,
    //         $meet->objective,
    //         $meet->state,
    //         $meet->observations,
    //         $meet->request_temp,
    //         $meet->created_at,
    //         $meet->participants()->get()->toArray(),
    //     ];
    // }

    // public function headings():Array{
    //     return [
    //         'Identificador',
    //         'Titulo',
    //         'Objectivo',
    //         'Estado',
    //         'Observaciones',
    //         'Se solicito temperatura?',
    //         "fecha de creación",
    //         "Participantes"
    //     ];
    // }

    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
        // return Meet::select(["id","topic","objective","state","observations","request_temp","created_at"])
        // ->where("created_at",">=",$this->datein)
        // ->where("created_at","<=",$this->dateout)
        // ->whereIn('user_id',$this->users)
        // ->get();
    // }
}
